<%@include file="cabeceras/principal.jsp" %>
<title>Core Networks</title>
</head>
<body>
	<div class="container">

		<div class="cabecera_vacio"></div>

		<div class="text-center">
			<img id="logo" src="./assets/img/core_logo.png" class="img-fluid"
				alt="Core networks logo">
		</div>
		
		<div class="row justify-content-md-center">
			<div class="col-12 col-md-6">
				<p class="h2 col text-center">Login</p>
				<!-- Form login -->
				<form:form modelAttribute="alumno" action="index.htm"
					class="form-control">
					<form:errors path="email" cssClass="error"></form:errors>
					<div class="md-form">
						<i class="fa fa-envelope prefix green-text"></i>
						<form:input type="email" id="email" class="form-control"
							path="email" placeholder="correo"></form:input>
					</div>

					<div class="md-form">
						<i class="fa fa-lock prefix green-text"></i>
						<form:input type="password" id="defaultForm-pass"
							class="form-control" path="pass" placeholder="contraseņa"></form:input>
					</div>

					<div class="text-center">
						<button class="btn btn-default">Login</button>
					</div>
					<div class="text-right">
						<button type="button" onclick="location.href = 'registro.htm'"
							class="btn btn-primary">Registrate</button>
					</div>
				</form:form>
			</div>
			<!-- Form login -->
		</div>
	</div>
</body>
</html>
