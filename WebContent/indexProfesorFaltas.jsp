<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="modelo.negocio.GestionProfesor"%>
<%@page import="entidades.*"%>
<%@page import="java.util.*"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro de Faltas</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link rel="stylesheet"
	href="https://unpkg.com/bootstrap-material-design@4.0.0-beta.3/dist/css/bootstrap-material-design.min.css">
<link rel="stylesheet" href="./assets/css/style.css">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://unpkg.com/popper.js@1.12.5/dist/umd/popper.js"></script>
<script
	src="https://unpkg.com/bootstrap-material-design@4.0.0-beta.3/dist/js/bootstrap-material-design.js"></script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$(document).ready(
			function() {
				$(function() {
					$('.datepicker').datepicker({
						dateFormat : 'dd/mm/yy',
						showButtonPanel : false,
						changeMonth : true,
						changeYear : true,
						/*showOn: "button",
						buttonImage: "images/calendar.gif",
						buttonImageOnly: true,
						minDate: '+1D',
						maxDate: '+3M',*/
						inline : true
					});
				});
				$.datepicker.regional['es'] = {
					closeText : 'Cerrar',
					prevText : '<Ant',
		    nextText: 'Sig>',
					currentText : 'Hoy',
					monthNames : [ 'Enero', 'Febrero', 'Marzo', 'Abril',
							'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre',
							'Octubre', 'Noviembre', 'Diciembre' ],
					monthNamesShort : [ 'Ene', 'Feb', 'Mar', 'Abr', 'May',
							'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
					dayNames : [ 'Domingo', 'Lunes', 'Martes', 'Mi�rcoles',
							'Jueves', 'Viernes', 'S�bado' ],
					dayNamesShort : [ 'Dom', 'Lun', 'Mar', 'Mi�', 'Juv', 'Vie',
							'S�b' ],
					dayNamesMin : [ 'Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S�' ],
					weekHeader : 'Sm',
					dateFormat : 'dd/mm/yy',
					firstDay : 1,
					isRTL : false,
					showMonthAfterYear : false,
					yearSuffix : ''
				};
				$.datepicker.setDefaults($.datepicker.regional['es']);
			});
</script>

</head>
<body>


	<div class="container">


		<!-- Definici�n de cabecera. -->
		<!-- ----------------------- -->
		<nav class="navbar navbar-expand-lg navbar-light bg-light"> <a
			class="navbar-brand" href="#">INICIO</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="text-right" class="collapse navbar-collapse"
			id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link" href="#">Profesor
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Cerrar sesi�n</a></li>

			</ul>
		</div>
		</nav>
		<%
			System.out.println("Cargando formulario");
		%>
		
		<!-- Definici�n del formulario. -->

		<form:form modelAttribute="faltarellenar"
			action="indexProfesorFaltas.htm" class="prueba">
			<form:hidden path="idCursoAlumno"/>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="fecha" class="col-form-label">Fecha de alta</label>
					<form:input type="text" class="form-control datepicker" id="fecha"
						placeholder="Fecha de alta dd/mm/aaaa" path="fecha"></form:input>
				</div>
				<div class="form-group col-md-6">
					<label for="motivo" class="col-form-label">Motivo</label>
					<form:input type="text" class="form-control" id="motivo"
						placeholder="Motivo" path="motivo"></form:input>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group">
					<label for="horas" class="col-form-label">Horas</label>
					<form:input type="number" class="form-control" id="horas"
						placeholder="1234 Main St" path="horas"></form:input>
				</div>

				<div class="form-group">
					<label for="justificado" class="col-form-label">Justificado</label>
					<form:checkbox class="form-check-input" id="justificado"
						path="justificado" value="1"></form:checkbox>

				</div>
			</div>


			<button class="btn btn-primary">Registrar / Actualizar</button>
			<a href="indexProfesorFaltas.htm">Registrar nueva falta</a>

		</form:form>

		<!-- Definici�n lista de datos. -->


		<table class="table table-inverse">
			<thead>
				<tr>

					<th>Fecha</th>
					<th>Horas</th>
					<th>Motivo</th>
					<th>Justificado</th>
				</tr>
			</thead>
			<%
				Cursoalumno cursoalumno= new Cursoalumno();
				List<Falta> faltas= new ArrayList<Falta>();;
// 				Falta faltas ;

				cursoalumno = (Cursoalumno)  request.getAttribute("Cursoalumno");
				faltas = (List<Falta>) request.getAttribute("Faltas");
				
				System.out.println("Cargando antes de lista");
			
				
// 				faltas = (Falta) request.getAttribute("falta");
				
// 				List<Profesor> profesores=(ArrayList)request.getAttribute("listaProfesores");
// 				for(Profesor profesor: profesores){
				for (Falta falta: faltas) {
				//for (Falta falta: cursoalumno.getFaltas()) {
					
					System.out.println("Cargando lista " + faltas.size());
				
			%>
			
			<tbody>
				<tr>

					<td class="listap"><a
						style='text-decoration: none; color: black;(otros)'
						href="indexProfesorFaltasVer.htm?idCursoAlumno=	
	<%=cursoalumno.getIdCursoAlumno()%>&idFaltas=<%=falta.getIdFaltas()%>"><%=falta.getFecha()%>
					</a></td>
					<td class="listap"><a
						style='text-decoration: none; color: white;(otros)'
						href="indexProfesorFaltasVer.htm?idCursoAlumno=
	<%=cursoalumno.getIdCursoAlumno()%>&idFaltas=<%=falta.getIdFaltas()%>"><%=falta.getHoras()%>
					</a></td>
					<td class="listap"><a
						style='text-decoration: none; color: white;(otros)'
						href="indexProfesorFaltasVer.htm?idCursoAlumno=
	<%=cursoalumno.getIdCursoAlumno()%>&idFaltas=<%=falta.getIdFaltas()%>"><%=falta.getMotivo()%>
					</a></td>
					<td class="listap"><a
						style='text-decoration: none; color: white;(otros)'
						href="indexProfesorFaltasVer.htm?idCursoAlumno=
	<%=cursoalumno.getIdCursoAlumno()%>&idFaltas=<%=falta.getIdFaltas()%>"><%=falta.getJustificado()%>
					</a></td>

				</tr>



				<%
					}
				%>
			
</body>
</html>