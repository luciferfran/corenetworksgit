<%@include file="cabeceras/principal.jsp"%>
<title>Registro de cursos</title>
</head>
<body>
	<%@include file="cabeceras/menuProfesor.jsp"%>
	<%
		Curso c = (Curso) request.getAttribute("Curso");
	%>
	<div class="container">
		<h1 class="text-center">Detalle del curso</h1>
		<div class="row">
			<div class="col-md-4">
				<h1>Curso</h1>
				<img class="img-fluid" alt="curso" src="<%=c.getImagen()%>">
				<ul class="list-group">
					<h3>Detalles del curso</h3>
					<li class="list-group-item">Profesor:<%=c.getProfesor().getNombre()%></li>
					<li class="list-group-item">Fecha de inicio:<%=c.getFechaInicio()%></li>
					<li class="list-group-item">Fecha final:<%=c.getFechaFin()%></li>
					<li class="list-group-item">Duraci�n:<%=c.getDuracion()%></li>
					<li class="list-group-item">Lugar:<%=c.getLugar()%></li>
					<li class="list-group-item">Especialidad:<%=c.getEspecialidad().getNombre()%></li>
					<li class="list-group-item">Descripcion:<%=c.getContenido()%></li>
				</ul>
			</div>
			<div class="col-md-8">
				<table class="table table-inverse">
					<thead>
						<tr>

							<th>Lista de alumno</th>
							<th>Faltas</th>
							<th>Notas</th>
							<th>Baja</th>
							<th>Matricula</th>
						</tr>
					</thead>
					<%
						List<Cursoalumno> cursoalumnos = (List<Cursoalumno>) request.getAttribute("ListaCursoalumnos");
						List<Integer> faltas = (ArrayList<Integer>) request.getAttribute("faltas");
						int indiceFaltas=-1;
						for (Cursoalumno ca : cursoalumnos) {
							indiceFaltas++;
					%>
					<tbody>
						<tr>
							<td class="listap"><a
								style='text-decoration: none; color: black;(otros)'
								href="indexProfesorFaltas.htm?idCursoAlumno=
	<%=ca.getIdCursoAlumno() %>"><%=ca.getAlumno().getNombre()%>
							</a></td>
							<td class="listap"><p><%=faltas.get(indiceFaltas) %></p></td>
							<td class="listap"><input type="number" min="0" max="0"
								value="<%=ca.getNota()%>"> </input></td>
							<td class="listap"><label class="switch"> <input
									type="checkbox" <%=(ca.getBaja() == (byte) 0) ? "" : "checked"%>>
									<span class="slider"></span>
							</label></td>
							<td class="listap"><label class="switch"> <input
									type="checkbox"
									<%=(ca.getMatriculado() == (byte) 0) ? "" : "checked"%>> <span
									class="slider"></span>
							</label></td>
						</tr>
						<%
							}
						%>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>