<%
	Alumno user = (Alumno) session.getAttribute("usuario");
%>

<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">

	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbar" aria-controls="navbarsExampleDefault"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbar">
		<a class="navbar-brand" href="#">Core NetWork</a>
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active"><a class="nav-link"
				href="./indexAlumno.htm">Inicio<span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item"><a class="nav-link"
				href="./indexAdminRegistroEspecialidad.htm">Especialidades</a></li>
			<li class="nav-item"><a class="nav-link"
				href="./indexAdminRegistroProfesor.htm">Profesores</a></li>
		</ul>
		<ul class="navbar-nav pull-right">
			<li class="nav-item .text-danger"><a class="nav-link"
				href="indexAlumnoPerfil.htm"><%=user.getApellidos()%>, <%=user.getNombre()%></a></li>
							<li class="nav-item"><a class="nav-link"
				href="./cerrarSesion.htm">Cerrar sesion</a></li>
		</ul>
	</div>
</nav>