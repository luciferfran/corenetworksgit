<%@include file="cabeceras/principal.jsp" %>
<title>Registro profesor</title>
</head>
<body>
<%@include file="cabeceras/menuAdmin.jsp" %>

<div class="container">
<form:form modelAttribute="profesor" action="indexAdminRegistroProfesor.htm" class="prueba">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4" class="col-form-label">Email</label>
      <form:input type="email" class="form-control" id="inputEmail4" placeholder="Email" path="email" 
     
    ></form:input>
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4" class="col-form-label">Password</label>
      <form:input type="password" class="form-control" id="inputPassword4" placeholder="Password" path="pass"></form:input>
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress" class="col-form-label">Nombre</label>
    <form:input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" path="nombre"></form:input>
  </div>
  <div class="form-group">
    <label for="inputAddress2" class="col-form-label">Apellidos</label>
    <form:input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor" path="apellidos"></form:input>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputCity" class="col-form-label">CV</label>
      <form:input type="text" class="form-control" id="inputCity" path="cv"></form:input>
    </div>
   
    <div class="form-group col-md-2">
      <label for="inputZip" class="col-form-label">Tel�fono</label>
      <form:input type="text" class="form-control" id="inputZip" path="tlf"></form:input>
    </div>
  </div>
  <button class="btn btn-primary">Registrar / Actualizar</button>
  <a href="indexAdminRegistroProfesor.htm">Registrar un nuevo profesor</a>
</form:form>

<table class="table table-inverse"> 
  <thead>
    <tr>
  
      <th>Nombre</th>
      <th>Apellidos</th>
      <th>Tel�fono</th>
      <th>Email</th>
    </tr>
    </thead>
<%
List<Profesor> profesores=(ArrayList)request.getAttribute("listaProfesores");
for(Profesor profesor: profesores){
%>
<tbody>
<tr>

		<td class="listap" >	<a style='text-decoration:none;color:black;(otros)' href="indexAdminVisualizarProfesor.htm?idProfesor=
	<%=profesor.getIdProfesor()%>"><%=profesor.getNombre()%>   </a>  </td>
		<td class="listap"><a style='text-decoration:none;color:white;(otros)' href="indexAdminVisualizarProfesor.htm?idProfesor=
	<%=profesor.getIdProfesor()%>"><%=profesor.getApellidos()%>    </a> </td>
		<td class="listap"><a style='text-decoration:none;color:white;(otros)' href="indexAdminVisualizarProfesor.htm?idProfesor=
	<%=profesor.getIdProfesor()%>"><%=profesor.getTlf()%>  </a> </td>
        <td class="listap"><a style='text-decoration:none;color:white;(otros)' href="indexAdminVisualizarProfesor.htm?idProfesor=
	<%=profesor.getIdProfesor()%>"><%=profesor.getEmail()%> </a> </td>
		
		</tr>
		
		
		
<%
}%>
</tbody>
</table>
</div>
</div>
</body>
</html>