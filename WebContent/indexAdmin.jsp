<%@include file="cabeceras/principal.jsp" %>
<title>Registro de cursos</title>
</head>
<body>
<%@include file="cabeceras/menuAdmin.jsp" %>
	<div class="container">
		<h1 class="text-center">Agregar nuevo curso</h1>
		<div class="row justify-content-center">
			<div class="col-md-8">
				<form:form modelAttribute="curso" action="indexAdmin.htm"
					method="post" cssClass="form-group" enctype="multipart/form-data">
					<form:hidden path="idCurso"/>
					<form:hidden path="imagen"/>
					<div class="form-row">
						<div class="form-group col-md-6">
							<form:input type="text" path="nombre"
								placeholder="Nombre del curso" cssClass="form-control"
								required="required" />
						</div>
						<div class="form-group col-md-6">
							<form:select items="${profesores}" itemValue="idProfesor"
								itemLabel="nombre" path="idProfesor"
								cssClass="custom-select w-100" />
						</div>
						<div class="form-group col-md-6">
							<form:input type="text" path="duracion" placeholder="Duración"
								cssClass="form-control" required="required" />
						</div>
						<div class="form-group col-md-6">
							<form:input type="text" path="lugar"
								placeholder="Lugar de impartición" cssClass="form-control"
								required="required" />
						</div>
						<div class="form-group col-md-6">
							<form:textarea path="contenido" placeholder="Contenido del curso"
								cssClass="form-control" rows="9" required="required" />
						</div>
						<div class="form-group col-md-6">
							<form:select items="${especialidades}" itemValue="idEspecialidad"
								itemLabel="nombre" path="idEspecialidad"
								cssClass="custom-select w-100" />
							<div class="md-form">
								<i class="fa fa-calendar"></i>
								<form:input type="text" id="fechaInicio"
									cssClass="form-control datepicker" path="fechaInicio"
									required="required" placeholder="Fecha inicio dd/mm/aaaa"></form:input>
							</div>
							<div class="md-form">
								<i class="fa fa-calendar prefix grey-text"></i>
								<form:input type="text" id="fechaFin"
									class="form-control datepicker" path="fechaFin"
									required="required" placeholder="Fecha fin dd/mm/aaaa"></form:input>
							</div>

						</div>

						<div class="form-group col-md-12"">
							<label for="file">Imágen del curso</label>
							<form:input path="subida" type="file"
								cssClass="form-control-file" />
						</div>
					</div>
					<button class="btn btn-primary">Registrar</button>
				</form:form>
			</div>

		</div>
		<p>Lista de cursos</p>
		<div class="row justify-content-center">
			<%
				@SuppressWarnings("unchecked")
				List<Curso> cursos = (List<Curso>) request.getAttribute("cursos");
				for (Curso curso : cursos) {
			%>
			<div class="col-md-4">
				<div class="card" style="width: 20rem;">
					<img class="card-img-top" src="<%=curso.getImagen()%>"
						alt="<%=curso.getNombre()%>">
					<div class="card-body">
						<h4 class="card-title"><%=curso.getNombre()%></h4>
						<p class="card-text"><%=curso.getContenido()%></p>
						<p>
							Inicio:
							<%=curso.getFechaInicio().toString().subSequence(0, 10)%></p>
						<a class="btn btn-primary"
							href="indexAdminVisualizarCurso.htm?idCurso=<%=curso.getIdCurso()%>">Modificar</a>
					</div>
				</div>
			</div>
			<%
				}
			%>
		</div>
	</div>
</body>
</html>