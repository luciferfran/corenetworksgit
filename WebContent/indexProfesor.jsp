<%@include file="cabeceras/principal.jsp"%>
<title>Bienvenido profesor</title>
</head>
<body>
	<%@include file="cabeceras/menuProfesor.jsp"%>
	<div class="container">
		<p>Lista de cursos actual</p>
		<div class="row justify-content-center">
			<%
				@SuppressWarnings("unchecked")
				//String urlImagenes = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath();
				List<Curso> cursos = (List<Curso>) request.getAttribute("cursosProfesor");
				for (Curso curso : cursos) {
			%>
			<div class="col-md-4">
				<div class="card" style="width: 20rem;">
					<a href="indexProfesorDetalleCurso.htm?idCurso=<%=curso.getIdCurso()%>">
						<img class="card-img-top" src="<%=curso.getImagen()%>"
						alt="<%=curso.getNombre()%>">
						<div class="card-body">
							<h4 class="card-title"><%=curso.getNombre()%></h4>
							<p class="card-text"><%=curso.getContenido()%></p>
							<p>
								Inicio:
								<%=curso.getFechaInicio().toString().subSequence(0, 10)%></p>
						</div>
					</a>
				</div>
			</div>
			<%
				}
			%>
		</div>
</body>
</html>