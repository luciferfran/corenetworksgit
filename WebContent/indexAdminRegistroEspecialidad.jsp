<%@include file="cabeceras/principal.jsp"%>
<title>Registro de especialidad</title>
</head>
<body>
	<%@include file="cabeceras/menuAdmin.jsp"%>
	<div class="container">
		<form:form modelAttribute="especialidad"
			action="indexAdminRegistroEspecialidad.htm" class="form-inline">
			<form:input type="text" path="nombre"
				placeholder="Nombre de la especialidad" cssClass="form-control"
				size="45" maxlength="45" />
			<button class="btn btn-primary">Grabar</button>
		</form:form>
		<table class="table table-inverse">
			<thead>
				<tr>

					<th>idEspecialidad</th>
					<th>Nombre</th>

				</tr>
			</thead>
			<%
				List<Especialidad> especialidades = (ArrayList) request.getAttribute("listaEspecialidades");
				for (Especialidad especialidad : especialidades) {
			%>
			<tbody>
				<tr>

					<td class="listap"><a
						style='text-decoration: none; color: black;(otros)'
						href="indexAdminVisualizarEspecialidad.htm?idEspecialidad=
	<%=especialidad.getIdEspecialidad()%>"><%=especialidad.getIdEspecialidad()%>
					</a></td>
					<td class="listap"><a
						style='text-decoration: none; color: white;(otros)'
						href="indexAdminVisualizarEspecialidad.htm?idEspecialidad=
	<%=especialidad.getIdEspecialidad()%>"><%=especialidad.getNombre()%>
					</a></td>

				</tr>
				<%
					}
				%>
			</tbody>
		</table>
	</div>
</body>
</html>