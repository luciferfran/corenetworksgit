<%@include file="cabeceras/principal.jsp" %>
<title>Registro</title>
</head>
<body>
	<div class="container">

		<div class="cabecera_vacio"></div>

		<div class="text-center">
			<img id="logo" src="./assets/img/core_logo.png" class="img-fluid"
				alt="Core networks logo">
		</div>

		<div class="row justify-content-md-center">
			<div class="col-12 col-md-6">
				<p class="h2 col text-center">Registro</p>

				<!-- Form login -->
				<form:form modelAttribute="alumno" class="form-control"
					action="registro.htm">
					<div class="md-form error">
						<form:errors path="email" cssClass="error">1</form:errors>
					</div>
					<div class="md-form">
						<i class="fa fa-user prefix grey-text"></i>
						<form:input type="text" id="nombre" class="form-control"
							path="nombre" required="required" placeholder="Nombre"></form:input>
					</div>

					<div class="md-form">
						<i class="fa fa-address-book prefix grey-text"></i>
						<form:input type="text" id="apellidos" class="form-control"
							path="apellidos" required="required" placeholder="Apellidos"></form:input>
					</div>

					<div class="md-form">
						<i class="fa fa-phone prefix grey-text"></i>
						<form:input type="number" id="telefono" class="form-control"
							path="tlf" placeholder="Tel�fono" min="0" max="999999999"></form:input>
					</div>
					<div class="md-form">
						<i class="fa fa-address-card prefix grey-text"></i>
						<form:input type="text" id="dni" class="form-control" path="dni"
							required="required" placeholder="DNI" maxlength="9"></form:input>
					</div>

					<div class="md-form">
						<i class="fa fa-calendar prefix grey-text"></i>
						<form:input type="text" id="fechaNacimiento" class="form-control datepicker"
							path="fechaNacimiento" required="required"
							placeholder="Fecha nacimiento dd/mm/aaaa"></form:input>
					</div>

					<div class="md-form">
						<i class="fa fa-envelope prefix grey-text"></i>
						<form:input type="email" id="email" class="form-control"
							path="email" required="required" placeholder="Correo"></form:input>
					</div>

					<div class="md-form">
						<i class="fa fa-lock prefix grey-text"></i>
						<form:input type="password" id="pass" class="form-control"
							path="pass" required="required" placeholder="Contrase�a"></form:input>
					</div>

					<div class="text-center">
						<button class="btn btn-default">Registrar</button>
					</div>
					<div class="text-right">
						<button type="button" onclick="location.href = 'login.htm'"
							class="btn btn-primary">Volver al login</button>
					</div>
				</form:form>
				<!-- Form login -->
			</div>
		</div>

	</div>

</body>
</html>
