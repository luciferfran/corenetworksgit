package controller;

import java.util.List;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


import entidades.Curso;
import entidades.Profesor;
import modelo.negocio.GestionCurso;
import modelo.negocio.GestionProfesor;

@Controller
public class ProfesorController {
	@Autowired
	GestionProfesor gestionP;
	@Autowired
	GestionCurso gestionC;
	@RequestMapping("/indexProfesor.htm")
	public String indexProfesor(Model model, HttpSession sesion) {
		sesion.setMaxInactiveInterval(1200);
		Profesor profe=(Profesor) sesion.getAttribute("usuario");
		List<Curso> cursosProfesor=(List<Curso>)gestionC.recuperarCursosProfesor(profe);
		model.addAttribute("cursosProfesor",cursosProfesor);
		return "indexProfesor";
		
	}

}
