package controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import entidades.Curso;
import entidades.Cursoalumno;
import modelo.negocio.GestionAlumno;
import modelo.negocio.GestionCurso;
import modelo.negocio.GestionCursoalumno;

@Controller
public class CursoAlumnoController {
	@Autowired
	GestionAlumno gA;
	@Autowired
	GestionCurso gC;
	@Autowired
	GestionCursoalumno gCa;
	
	@RequestMapping("/indexProfesorDetalleCurso.htm")
	public String mostrarCursoAlumno(@RequestParam("idCurso")int idCurso, Model modelo) {
		
		Curso c=gC.recuperarCurso(idCurso);
		
		List<Cursoalumno> ListaCursoalumnos= c.getCursoalumnos();
		modelo.addAttribute("ListaCursoalumnos", ListaCursoalumnos);
		List<Integer> faltas = new ArrayList<>();
		
		for (Cursoalumno cursoAlumno: ListaCursoalumnos) {
			faltas.add(gCa.recuperarNumFaltas(cursoAlumno));
		}
		
		modelo.addAttribute("Curso", c);

		modelo.addAttribute("faltas", faltas);
		
		
//		for (Cursoalumno cursoalumno : ListaCursoalumnos) {
//			System.out.println(cursoalumno.getAlumno().getNombre()+" "+cursoalumno.getNota()+" "+cursoalumno.getMatriculado()+" "+cursoalumno.getBaja());
//		}
		
		return "indexProfesorDetalleCurso";
	}
}
