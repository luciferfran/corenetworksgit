package controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import entidades.Alumno;
import entidades.Profesor;
import modelo.negocio.GestionAlumno;
import modelo.negocio.GestionProfesor;

@Controller
public class UsuarioController {
	@Autowired
	GestionAlumno gestionA;
	@Autowired
	GestionProfesor gestionP;


	/************* Controller de registro ***************************/
	@RequestMapping("/registro.htm")
	public String rellenarFormUsuario(Model modelo) {
		modelo.addAttribute("alumno", new Alumno());
		return "registro";
	}

	@PostMapping("/registro.htm")
	public String registrarAlumno(@ModelAttribute("usuario") Alumno a, BindingResult errores) {
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//		Date date = Date.parse(usuario.getFechaNacimiento().toString(), formatter);
//		usuario.setFechaNacimiento(date);
		if (!errores.hasErrors()) {
			if (!gestionA.registrarAlumno(a)) {
				errores.rejectValue("email", "repetido");
				return "registro";
			} else {
				return "redirect:/login.htm";
			}
		}
		return "registro";
	}
	// Fin controller registro

	/******************* Controller login**************************** */
	@RequestMapping("/login.htm")
	public String rellenarForm(Model modelo) {
		modelo.addAttribute("alumno", new Alumno());
		return "login";
	}
	
	

	/******************* Controller index**************************** */

//	@RequestMapping(value = "/index.do", method = RequestMethod.GET)
//	public String indexUsuario(@ModelAttribute("usuario") Usuario user, HttpServletRequest request) {
//		return "index";
//
//	}
	
	@RequestMapping(value = "/index.htm", method = RequestMethod.POST)
	public String logarUsuario(@ModelAttribute("usuario") Alumno user, BindingResult binding,HttpServletRequest request) {
		
		if (!binding.hasErrors()) {
			Profesor p = null;
			Alumno a = gestionA.logarAlumno(user);
			if (a == null) {
				p = gestionP.logarProfesor(user);
			}
			
			if (a != null) {
				HttpSession sesion = request.getSession(true);
				sesion.setMaxInactiveInterval(1200);
//				a = gestionA.recuperarAlumno(a.getEmail());
				sesion.setAttribute("usuario", a);
				sesion.setAttribute("tipo", "alumno");
				return "redirect:/indexAlumno.htm";
				
			} else if ((p != null) &&!(p.getEmail().equals("admin@server.com"))) {
				HttpSession sesion = request.getSession(true);
				sesion.setMaxInactiveInterval(1200);
//				p = gestionP.recuperarProfesor(p.getEmail());
				sesion.setAttribute("usuario", p);
				sesion.setAttribute("tipo", "profesor");
				return "redirect:/indexProfesor.htm";

			} else if ((p != null) && (p.getEmail().equals("admin@server.com"))) {
				HttpSession sesion = request.getSession(true);
				sesion.setMaxInactiveInterval(1200);
//				p = gestionP.recuperarProfesor(p.getEmail());
				sesion.setAttribute("usuario", p);
				sesion.setAttribute("tipo", "administrador");
				return "redirect:/indexAdmin.htm";

			} else {
				binding.rejectValue("email", "errorLogin");
				return "redirect:/login.htm";
			}
		}
		
		return "redirect:/login.htm";
  }
	// Fin controller login

	@RequestMapping("/cerrarSesion.htm")
	public String cerrarSesion(HttpServletRequest request) {
		HttpSession sesion = request.getSession(false);
		if (sesion != null) {
			sesion.removeAttribute("usuario");
			sesion.invalidate();
		}
		return "redirect:/login.htm";
	}

//	@RequestMapping(value = "/agregarArtista.do", method = RequestMethod.POST)
//	public String agrearArtista(@ModelAttribute("usuario") Usuario usuario, Artista artista) {
//		gestion.aniadirArtista(usuario, artista.getIdArtista());
//		return "";
//	}

//	@RequestMapping(value = "/quitarArtista.do", method = RequestMethod.POST)
//	public String eliminarArtista(@ModelAttribute("usuario") Usuario usuario, Artista artista) {
//		gestion.eliminarArtista(usuario, artista.getIdArtista());
//		return "";
//	}



	public GestionAlumno getGestion() {
		return gestionA;
	}

	public void setGestion(GestionAlumno gestion) {
		this.gestionA = gestion;
	}

}
