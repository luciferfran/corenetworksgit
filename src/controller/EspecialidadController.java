package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import entidades.Especialidad;
import modelo.negocio.GestionEspecialidad;

@Controller
@SessionAttributes(names= {"especialidad"})
public class EspecialidadController {
	@Autowired
	GestionEspecialidad gestionE;

	/************* Controller de registrarEspecialidad ***************************/
	@RequestMapping("/indexAdminRegistroEspecialidad.htm")
	public String rellenarFormEspecialidad(Model modelo) {
		System.out.println("Hola Mundo!");
		modelo.addAttribute("especialidad", new Especialidad());
		modelo.addAttribute("listaEspecialidades", gestionE.recuperarEspecialidades());
		return "indexAdminRegistroEspecialidad";
	}

	@PostMapping("/indexAdminRegistroEspecialidad.htm")
	public String registrarEspecialidad(@ModelAttribute("especialidad") Especialidad e, BindingResult errores) {
		System.out.println("Hola Mundo2!");
		if (!errores.hasErrors()) {
			if (!gestionE.registrarEspecialidad(e)) {
				errores.rejectValue("especialidad", "EspecialidadFalta");
				return "indexAdminRegistroEspecialidad";
			} else {

				return "redirect:/indexAdminRegistroEspecialidad.htm";
			}
		}
		return "indexAdminRegistroEspecialidad";
	}
	// Fin controller registro

	@RequestMapping("/indexAdminVisualizarEspecialidad.htm")
	public String detalleProfesor(@RequestParam("idEspecialidad") int id, Model model) {
		Especialidad e = gestionE.recuperarEspecialidad(id);
		model.addAttribute("especialidad", e);
		model.addAttribute("listaEspecialidades", gestionE.recuperarEspecialidades());
		return "indexAdminRegistroEspecialidad";
	}
}
