package controller;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import entidades.Curso;
import entidades.CursoRellenar;
import entidades.Especialidad;
import entidades.Profesor;
import modelo.negocio.GestionCurso;
import modelo.negocio.GestionEspecialidad;
import modelo.negocio.GestionProfesor;

@Controller
@EnableWebMvc
@SessionAttributes(names = { "alumno" })
public class CursoController2 {
	@Autowired
	GestionCurso gestionC;

	@Autowired
	GestionProfesor gestionP;

	@Autowired
	GestionEspecialidad gestionE;

	private static final String[] ALLOWED_FILE_TYPES = { "image/jpeg", "image/jpg", "image/gif" };
	private static final Long MAX_FILE_SIZE = 10485760L; // 10MB
	private static final String UPLOAD_FILE_PATH = "./assets/subidas/";

	/************* Controller de admin ***************************/
	@RequestMapping("/indexAdmin2.htm")
	public String rellenarFormUsuario(Model modelo) {
		System.out.println("Inicio formularios");

		modelo.addAttribute("curso", new Curso());

		List<Profesor> misProfesores = gestionP.recuperarProfesores();
		List<Especialidad> misEspecialidades = gestionE.recuperarEspecialidades();
		List<Curso> misCursos = gestionC.recuperarCursos();

		Map<String, String> profesores = new HashMap<>();
		Map<String, String> especialidades = new HashMap<>();

		for (Profesor profesor : misProfesores) {
			profesores.put(String.valueOf(profesor.getIdProfesor()), profesor.getNombre());
		}
		for (Especialidad especialidad : misEspecialidades) {
			especialidades.put(String.valueOf(especialidad.getIdEspecialidad()), especialidad.getNombre());
		}

		modelo.addAttribute("profesores", profesores);
		modelo.addAttribute("especialidades", especialidades);
		modelo.addAttribute("cursos", misCursos);

		return "indexAdmin2";
	}

	@RequestMapping("/indexAdminVisualizarCurso2.htm")
	public String visualizarCurso(@RequestParam("idCurso") int id, Model modelo) {
		Curso cursoRecuperado = gestionC.recuperarCurso(id);
		CursoRellenar curso= new CursoRellenar();
		
		curso.setContenido(cursoRecuperado.getContenido());
		curso.setDuracion(cursoRecuperado.getDuracion());
		curso.setFechaFin(cursoRecuperado.getFechaFin());
		curso.setFechaInicio(cursoRecuperado.getFechaInicio());
		curso.setIdCurso(cursoRecuperado.getIdCurso());
		curso.setIdEspecialidad(cursoRecuperado.getEspecialidad().getIdEspecialidad());
		curso.setIdProfesor(cursoRecuperado.getProfesor().getIdProfesor());
		curso.setImagen(cursoRecuperado.getImagen());
		curso.setLugar(cursoRecuperado.getLugar());
		curso.setNombre(cursoRecuperado.getNombre());
		
		List<Profesor> misProfesores = gestionP.recuperarProfesores();
		List<Especialidad> misEspecialidades = gestionE.recuperarEspecialidades();
		List<Curso> misCursos = gestionC.recuperarCursos();

		Map<String, String> profesores = new HashMap<>();
		Map<String, String> especialidades = new HashMap<>();

		for (Profesor profesor : misProfesores) {
			profesores.put(String.valueOf(profesor.getIdProfesor()), profesor.getNombre());
		}
		for (Especialidad especialidad : misEspecialidades) {
			especialidades.put(String.valueOf(especialidad.getIdEspecialidad()), especialidad.getNombre());
		}

		modelo.addAttribute("profesores", profesores);
		modelo.addAttribute("especialidades", especialidades);
		modelo.addAttribute("cursos", misCursos);
		modelo.addAttribute("curso", curso);
		return "indexAdmin2";
	}

	@PostMapping("/indexAdmin2.htm")
	public String registrarCurso(@ModelAttribute("curso") Curso curso, BindingResult errores) {

		//Profesor profesor = gestionP.recuperarProfesor(curso.getIdProfesor());
		//Especialidad especialidad = gestionE.recuperarEspecialidad(curso.getIdEspecialidad());
		Curso nuevoCurso = new Curso();
//		nuevoCurso.setProfesor(profesor);
//		nuevoCurso.setEspecialidad(especialidad);
		nuevoCurso.setContenido(curso.getContenido());
		nuevoCurso.setDuracion(curso.getDuracion());
		nuevoCurso.setFechaFin(curso.getFechaFin());
		nuevoCurso.setFechaInicio(curso.getFechaInicio());
		nuevoCurso.setLugar(curso.getLugar());
		nuevoCurso.setNombre(curso.getNombre());
//		nuevoCurso.setImagen(UPLOAD_FILE_PATH + curso.getSubida().getOriginalFilename());

		try {
			gestionC.registrarCurso(nuevoCurso);
		} catch (Exception e) {
			errores.rejectValue("idProfesor", "falta");
			return "indexAdminRegistroCursoCorrecto";
		}

//		try {
//			MultipartFile file = curso.getSubida();
//			if (!file.isEmpty()) {
//				String newFile = "./assets/subidas/" + curso.getSubida().getOriginalFilename();
//				file.transferTo(new File(newFile));
//			} else {
//				return "Error. No hay archivo.";
//			}
//		} catch (Exception e) {
//		}

		return "indexAdminRegistroCursoCorrecto";
	}

}
