package controller;

import java.io.File;

import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import entidades.Curso;
import entidades.CursoImagen;
import entidades.CursoRellenar;
import entidades.Especialidad;
import entidades.Profesor;
import modelo.negocio.GestionCurso;
import modelo.negocio.GestionEspecialidad;
import modelo.negocio.GestionProfesor;

@Controller
@EnableWebMvc
@SessionAttributes(names = { "alumno" })
public class CursoController {
	@Autowired
	GestionCurso gestionC;

	@Autowired
	GestionProfesor gestionP;

	@Autowired
	GestionEspecialidad gestionE;

	@Autowired
	ServletContext context;
	// Para posible validaci�n de datos ahora no usada
	// private static final String[] ALLOWED_FILE_TYPES = { "image/jpeg",
	// "image/jpg", "image/gif" };
	// Para posible validaci�n de tama�o ahora no usado
	// private static final Long MAX_FILE_SIZE = 10485760L; // 10MB
	private static final String UPLOAD_FILE_PATH = "./assets/subidas/";

	/************* Controller de admin ***************************/
	@RequestMapping("/indexAdmin.htm")
	public String rellenarFormUsuario(Model modelo) {
		System.out.println("Inicio formularios");

		modelo.addAttribute("curso", new CursoRellenar());
		modelo.addAttribute("imagen", new CursoImagen());
		List<Profesor> misProfesores = gestionP.recuperarProfesores();
		List<Especialidad> misEspecialidades = gestionE.recuperarEspecialidades();
		List<Curso> misCursos = gestionC.recuperarCursos();

		modelo.addAttribute("profesores", misProfesores);
		modelo.addAttribute("especialidades", misEspecialidades);
		modelo.addAttribute("cursos", misCursos);

		return "indexAdmin";
	}

	@RequestMapping("/indexAdminVisualizarCurso.htm")
	public String visualizarCurso(@RequestParam("idCurso") int id,
			@ModelAttribute("curso") CursoRellenar cursoRellenado, Model modelo) {
		Curso cursoRecuperado = gestionC.recuperarCurso(id);
		List<Profesor> misProfesores = gestionP.recuperarProfesores();
		List<Especialidad> misEspecialidades = gestionE.recuperarEspecialidades();
		List<Curso> misCursos = gestionC.recuperarCursos();

		cursoRellenado.setContenido(cursoRecuperado.getContenido());
		cursoRellenado.setCursoalumnos(cursoRecuperado.getCursoalumnos());
		cursoRellenado.setDuracion(cursoRecuperado.getDuracion());
		cursoRellenado
				.setEspecialidad(gestionE.recuperarEspecialidad(cursoRecuperado.getEspecialidad().getIdEspecialidad()));
		cursoRellenado.setIdEspecialidad(cursoRecuperado.getEspecialidad().getIdEspecialidad());
		cursoRellenado.setFechaFin(cursoRecuperado.getFechaFin());
		cursoRellenado.setFechaInicio(cursoRecuperado.getFechaFin());
		cursoRellenado.setProfesor(gestionP.recuperarProfesor(cursoRecuperado.getProfesor().getIdProfesor()));
		cursoRellenado.setLugar(cursoRecuperado.getLugar());
		cursoRellenado.setIdEspecialidad(cursoRellenado.getIdEspecialidad());
		cursoRellenado.setIdProfesor(cursoRecuperado.getProfesor().getIdProfesor());
		cursoRellenado.setNombre(cursoRecuperado.getNombre());
		cursoRellenado.setImagen(cursoRecuperado.getImagen());

		modelo.addAttribute("curso", cursoRellenado);
		modelo.addAttribute("profesores", misProfesores);
		modelo.addAttribute("especialidades", misEspecialidades);
		modelo.addAttribute("cursos", misCursos);
		return "indexAdmin";
	}

	@PostMapping("/indexAdmin.htm")
	public String registrarCurso(@ModelAttribute("curso") CursoRellenar curso, BindingResult errores) {
		System.out.println("Entrando en POST indexAdmin");
		Profesor profesor = gestionP.recuperarProfesor(curso.getIdProfesor());
		Especialidad especialidad = gestionE.recuperarEspecialidad(curso.getIdEspecialidad());
		Curso nuevoCurso;
		if (curso.getIdCurso() == 0)
			nuevoCurso = new Curso();
		else
			nuevoCurso = gestionC.recuperarCurso(curso.getIdCurso());
		nuevoCurso.setProfesor(profesor);
		nuevoCurso.setEspecialidad(especialidad);
		nuevoCurso.setContenido(curso.getContenido());
		nuevoCurso.setDuracion(curso.getDuracion());
		nuevoCurso.setFechaFin(curso.getFechaFin());
		nuevoCurso.setFechaInicio(curso.getFechaInicio());
		nuevoCurso.setLugar(curso.getLugar());
		nuevoCurso.setNombre(curso.getNombre());

		try {
			MultipartFile file = curso.getSubida();
			if (!file.isEmpty()) {
				String newFile = context.getRealPath("") + File.separator + UPLOAD_FILE_PATH
						+ curso.getSubida().getOriginalFilename();
				file.transferTo(new File(newFile));
				nuevoCurso.setImagen(UPLOAD_FILE_PATH + curso.getSubida().getOriginalFilename());
			} else {
				nuevoCurso.setImagen(curso.getImagen());
			}
		} catch (Exception e) {
			System.out.println("No guard� imagen");
		}
		gestionC.registrarCurso(nuevoCurso);

		return "indexAdminRegistroCursoCorrecto";
	}

}
