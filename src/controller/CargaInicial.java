package controller;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;


import entidades.Alumno;
import entidades.AlumnoHasEspecialidad;
import entidades.Curso;
import entidades.Especialidad;
import entidades.Cursoalumno;
import entidades.Falta;
import entidades.Profesor;

import modelo.negocio.GestionEspecialidad;
import modelo.negocio.GestionFalta;
import modelo.negocio.GestionProfesor;
import modelo.negocio.GestionAlumno;
import modelo.negocio.GestionAlumnoHasEspecialidad;
import modelo.negocio.GestionCurso;
import modelo.negocio.GestionCursoalumno;

@Controller
@SessionAttributes(names={"profesor"})
public class CargaInicial {
	@Autowired
	GestionEspecialidad gestionE;
	@Autowired
	GestionProfesor     gestionP;
	@Autowired
	GestionCurso     gestionC;
	@Autowired
	GestionAlumno gestionA;
	@Autowired
	GestionAlumnoHasEspecialidad gestionAHE;
	@Autowired
	GestionCursoalumno gestionCa;
	@Autowired
	GestionFalta gestionF;
	
	
	/************* Controller de registrarEspecialidad ***************************/
	@RequestMapping("/cargaInicial.htm")
	public String rellenarcargaInicial(Model modelo) {	
		System.out.println("Hola Mundo!");
		Especialidad especialidad = new Especialidad();
		modelo.addAttribute("especialidad", especialidad);
		return "cargaInicial";
	}

	@PostMapping("/cargaInicial.htm")
	public String registrarcargaInicial(@ModelAttribute("especialidad") Especialidad esp, BindingResult errores) {
		System.out.println("Cargando especialidad");
		
		for (int i = 1; i < 10; i++) {
			Especialidad e = new Especialidad();
			e.setNombre("Especialidad " + i);	
			gestionE.registrarEspecialidad(e);
		}
		
		System.out.println("FIN Cargando especialidad");

		System.out.println("Cargando profesor");
		Profesor p1 = new Profesor();
		p1.setNombre("Administrador");
		p1.setApellidos("Apellidos administrador");
		p1.setCv("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula est et nulla imperdiet, vehicula feugiat augue malesuada. Aenean luctus nisi semper dolor sodales, nec condimentum neque volutpat.");
		p1.setTlf("999999999");
		p1.setEmail("admin@server.com");
		p1.setPass("admin");
		gestionP.registrarProfesor(p1);
		
		
		
		for (int i = 1; i < 10; i++) {
			Profesor p = new Profesor();
			p.setNombre("Profesor " + i);
			p.setApellidos("Apellidos Profesor " + i);
			p.setCv("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula est et nulla imperdiet, vehicula feugiat augue malesuada. Aenean luctus nisi semper dolor sodales, nec condimentum neque volutpat.");
			p.setTlf("999999999");
			p.setEmail("prof" + i + "@server.com");
			p.setPass("prof" +i);
			gestionP.registrarProfesor(p);
		}
		
		
		
		System.out.println("FIN Cargando profesor");
		
		System.out.println("Cargando profesor_has_especialidad");

		for (int i = 1; i < 10; i++) {
			Profesor p = new Profesor();
			p = gestionP.recuperarProfesor("prof" + i + "@server.com");
			List<Especialidad> especialidads = new ArrayList<Especialidad>() ;
			for (int l = 1; l < 10; l++) {
				
				Especialidad e = new Especialidad();
				e=gestionE.recuperarEspecialidad("Especialidad " + l);
				especialidads.add(e);
			}
			p.setEspecialidads(especialidads);
			gestionP.modificarProfesor(p);
			
		}
		
		System.out.println("FIN Cargando profesor_has_especialidad");
		
		
		
		System.out.println("Cargando Cursos");

		for (int i = 1; i < 10; i++) {
			Profesor p = new Profesor();
			p = gestionP.recuperarProfesor("prof" + i + "@server.com");
			
			Especialidad e = new Especialidad();
			e=gestionE.recuperarEspecialidad("Especialidad " + i);
			
			Curso c = new Curso();
			c.setNombre("Curso " + i);
			c.setDuracion("3 semanas y 40 horas de pr�cticas");
			c.setContenido("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula est et nulla imperdiet, vehicula feugiat augue malesuada. Aenean luctus nisi semper dolor sodales, nec condimentum neque volutpat.");
			c.setLugar("Lugar " + i);
			SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
			String strFecha = "2017-01-25";
			Date fecha = null;
			try {
				fecha = formatoDelTexto.parse(strFecha);
			} catch (ParseException exFecha1) {
				// TODO Auto-generated catch block
				exFecha1.printStackTrace();
			}
			c.setFechaInicio(fecha);
			strFecha = "2017-02-25";
			try {
				fecha = formatoDelTexto.parse(strFecha);
			} catch (ParseException exFecha1) {
				// TODO Auto-generated catch block
				exFecha1.printStackTrace();
			}
			
			c.setFechaFin(fecha);
			c.setImagen("https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Sin_foto.svg/300px-Sin_foto.svg.png");
			c.setProfesor(p);
			c.setEspecialidad(e);
			gestionC.registrarCurso(c);
		}
		
		System.out.println("FIN Cargando Cursos");
		
		
		
		System.out.println("Cargando Alumno");

		for (int i = 1; i < 100; i++) {
			Alumno a = new Alumno();
			a.setNombre("Alumno " + i);
			a.setApellidos("Apellidos Alumno " + i);
			
			SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
			String strFecha = "1980-01-25";
			Date fecha = null;
			try {
				fecha = formatoDelTexto.parse(strFecha);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			a.setFechaNacimiento(fecha);
			a.setTlf("999999999");
			a.setDni("123456789");
			a.setEmail("alumno" + i + "@server.com");
			a.setPass("alumno" + i);
			gestionA.registrarAlumno(a);
		}
		
		System.out.println("FIN Cargando Alumno");
		
		
		System.out.println("Cargando Alumno-Especialidad");
		for (int i = 1; i < 20; i++) {
			Alumno a = new Alumno();
			//List<AlumnoHasEspecialidad> alumnoHasEspecialidads = new ArrayList<AlumnoHasEspecialidad>();
			a = gestionA.recuperarAlumno("alumno" + i + "@server.com");
			for (int k = 1; k < 10; k++) {
				
				Especialidad e = new Especialidad();
				e=gestionE.recuperarEspecialidad("Especialidad " + k);
				
				//System.out.println("Especialidad recuperada " + k);
				
				AlumnoHasEspecialidad alumnoHasEspecialidad = new AlumnoHasEspecialidad ();
				alumnoHasEspecialidad.setAlumno(a);
				alumnoHasEspecialidad.setEspecialidad(e);
				
				
				
				gestionAHE.registrarAlumnoHasEspecialidad(alumnoHasEspecialidad);
			}
			
		
		}
		System.out.println("FIN Cargando Alumno-Especialidad");
		
		
		System.out.println("Cargando Curso-Alumno");
		
		for (int i = 1; i < 10; i++) {
			Curso c=new Curso();
			c=gestionC.recuperarCurso("Curso " + i);
			for (int j = 1; j < 30; j++) {
				Cursoalumno ca = new Cursoalumno();
				ca.setNota(0);
				ca.setBaja((byte) 0);
				ca.setMatriculado((byte) 1);
				SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
				String strFecha = "2017-01-25";
				Date fecha = null;
				try {
					fecha = formatoDelTexto.parse(strFecha);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				java.sql.Timestamp sqlStartDate = new java.sql.Timestamp(fecha.getTime());
				
				ca.setFechaRegistro(sqlStartDate);
				
				Alumno a= new Alumno();
				a = gestionA.recuperarAlumno("alumno" + j + "@server.com");
				
				
				
				ca.setAlumno(a);
				ca.setCurso(c);
				
				
				gestionCa.registrarCursoalumno(ca);
			}
		}
		System.out.println("FIN Cargando Curso-Alumno");

		System.out.println("Cargando Faltas");
		for (int i = 1; i < 10; i++) {
			Curso c=new Curso();
			c=gestionC.recuperarCurso("Curso " + i);
			
			System.out.println("Nombre curso: " + c.getNombre());
			
			for (int j = 0; j < c.getCursoalumnos().size(); j++) {
				if ((j % 3) == 0)
				{
					Cursoalumno ca = new Cursoalumno();
					ca = c.getCursoalumnos().get(j);
					
					Falta f= new Falta();					
					
					SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
					String strFecha = "2017-01-25";
					Date fecha = null;
					try {
						fecha = formatoDelTexto.parse(strFecha);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					f.setFecha(fecha);
					f.setHoras(2);
					f.setMotivo("Motivo 1");
					f.setJustificado((byte)1);
					f.setCursoalumno(ca);
					gestionF.registrarFalta(f);							
				}
			}
			
		}
		
		System.out.println("FIN Cargando Faltas");
		
		System.out.println("FIN proceso carga");
		return "cargaInicial";
	}
}