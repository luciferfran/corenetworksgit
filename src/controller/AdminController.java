package controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import entidades.Alumno;
import entidades.Profesor;
import modelo.negocio.GestionAlumno;
import modelo.negocio.GestionProfesor;

@Controller
@SessionAttributes(names={"profesor"})
public class AdminController {

	@Autowired
	GestionProfesor gestionP;

	@RequestMapping("/indexAdminRegistroProfesor.htm")
	public String rellenarFormUsuario(Model modelo) {
		modelo.addAttribute("profesor", new Profesor());
		
		modelo.addAttribute("listaProfesores",gestionP.recuperarProfesores());
		
		
		return "indexAdminRegistroProfesor";
	}
	
	
	@PostMapping("/indexAdminRegistroProfesor.htm")
	public String registrar(@ModelAttribute("profesor") Profesor profesor, BindingResult errores) {
		if (!errores.hasErrors()) {
			if (!gestionP.registrarProfesor(profesor)) {
				errores.rejectValue("email", "repetido");
				return "indexAdminRegistroProfesor";
			} else {
				return "redirect:/indexAdminRegistroProfesor.htm";
			}
		}
		return "indexAdminRegistroProfesor";
	}
	@RequestMapping("/indexAdminVisualizarProfesor.htm")
	public String detalleProfesor(@RequestParam("idProfesor")int id,Model model) {
		Profesor p=gestionP.recuperarProfesor((Integer)id);
		model.addAttribute("profesor", p);
		model.addAttribute("listaProfesores",gestionP.recuperarProfesores());
		return "indexAdminRegistroProfesor";
	}
//	@RequestMapping("/modificarProfesor.htm")
//	public String modificar(@ModelAttribute("idProfesor") int id,Model model) {
//		Profesor p=gestionP.recuperarProfesor((Integer)id);
//		model.addAttribute("profesor", p);
//		model.addAttribute("listaProfesores",gestionP.recuperarProfesores());
//		return "registrarProfesor";
//	}
//	
//	@PostMapping("/modificarProfesor.htm")
//	public String modificarProfesor(@ModelAttribute("idProfesor") int id,Model model) {
//		Profesor p=gestionP.recuperarProfesor((Integer)id);
//		gestionP.modificarProfesor(p.getEmail());
//		model.addAttribute("profesor", p);
//		return "registrarProfesor";
//	}
	
	
}
