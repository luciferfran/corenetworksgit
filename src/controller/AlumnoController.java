package controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.GenericStoredProcedure;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import entidades.Alumno;
import entidades.AlumnoHasEspecialidad;
import entidades.Curso;
import entidades.CursoAlumnoRellenar;
import entidades.Cursoalumno;
import entidades.Profesor;
import modelo.negocio.GestionAlumno;
import modelo.negocio.GestionCurso;
import modelo.negocio.GestionCursoalumno;
import modelo.negocio.GestionEspecialidad;



@Controller
@SessionAttributes(names={"alumno"})
public class AlumnoController {

		@Autowired
		GestionAlumno gestionA;
		@Autowired
		GestionCurso gestionC;
		@Autowired
		GestionCursoalumno gestionM;
		@Autowired
		GestionEspecialidad gestionE;
		
	@RequestMapping("/indexAlumnoMatricularCurso.htm")
	public String mostrarCursoAlumno(@RequestParam("idCurso")int idC, Model modelo,HttpServletRequest request) {
		
		HttpSession sesion = request.getSession(false);
		sesion.setMaxInactiveInterval(1200);
		Alumno a=(Alumno) sesion.getAttribute("usuario");
		Curso c=gestionC.recuperarCurso(idC);
		modelo.addAttribute("curso",c);
		modelo.addAttribute("alumno",a);
		return "indexAlumnoMatricularCurso";
	}
		
	@PostMapping("/indexAlumnoMatricularCurso.htm")
	public String matricular(@RequestParam("idCurso")int idC,HttpServletRequest request) {
		HttpSession sesion = request.getSession(false);
		//sesion.setMaxInactiveInterval(1200);
		Alumno a=(Alumno) sesion.getAttribute("usuario");
		//Alumno b=gestionA.recuperarAlumno(a.getIdalumno());
		Cursoalumno Ca = new Cursoalumno();
		Ca.setAlumno(a);
		Ca.setCurso(gestionC.recuperarCurso(idC));
		Ca.setNota(10);
		Ca.setBaja((byte) 0);
		//Ca.setAlumno(b);
		java.util.Date fechaRegistro = new Date();
    	java.sql.Timestamp sqlStartDate = new java.sql.Timestamp(fechaRegistro.getTime());
    	Ca.setFechaRegistro(sqlStartDate);
		System.out.println(Ca);
		gestionM.registrarCursoalumno(Ca);
		
//		int numMatriculasTotal=1;
////		int numMatriculasTotal = gestionM.numeroMatriculasTotales(c.getIdCurso()).size();
//		int numMatriculasVacantes=0;
//		Cursoalumno alumno=new Cursoalumno();
//		
//		for (int j = 0; j < numMatriculasTotal; j++) {
//			if ((byte)alumno.getMatriculado()==0) {
//				numMatriculasVacantes+=1;
//			}
//		}
//		
//		if(numMatriculasVacantes<20) {
//			matricula.setAlumno(a);
//			matricula.setCurso(gestionC.recuperarCurso(c.getIdCurso()));
//			java.util.Date fechaRegistro = new Date();
//			java.sql.Timestamp sqlStartDate = new java.sql.Timestamp(fechaRegistro.getTime());
//			matricula.setFechaRegistro(sqlStartDate);
//			gestionM.registrarCursoalumno(matricula);
//		}
		return "indexAlumno";
	}
		
	
	@RequestMapping("/indexAlumnoPerfil.htm")
	public String mostrarEspecialidades(Model modelo, HttpServletRequest request) {
		HttpSession sesion = request.getSession(false);
		sesion.setMaxInactiveInterval(1200);
		Alumno a=(Alumno) sesion.getAttribute("usuario");
	modelo.addAttribute("listaEspecialidades1", gestionE.recuperarEspecialidades());
	modelo.addAttribute("listaEspecialidades2", gestionA.recuperarAlumno(a.getIdalumno()).getAlumnoHasEspecialidads());
	System.out.println(gestionE.recuperarEspecialidades());
	System.out.println(gestionA.recuperarAlumno(a.getIdalumno()).getAlumnoHasEspecialidads());
		return "indexAlumnoPerfil";
	}
	@PostMapping("/indexAlumnoPerfil.htm")
	public String actualizarEspecialidades(@ModelAttribute("somedata") List<AlumnoHasEspecialidad> especialidads,HttpServletRequest request) {
		HttpSession sesion = request.getSession(false);
		sesion.setMaxInactiveInterval(1200);
		Alumno a=(Alumno) sesion.getAttribute("usuario");
		return "indexAlumnoPerfil";
	}
	
	@RequestMapping("/indexAlumno.htm")
	public String indexAlumno(Model model,HttpServletRequest request) {
		HttpSession sesion = request.getSession(false);
		sesion.setMaxInactiveInterval(1200);
		Alumno a=(Alumno) sesion.getAttribute("usuario");
		List<Curso> cursos=(List<Curso>)gestionC.recuperarCursos();
		model.addAttribute("cursos",cursos);
		return "indexAlumno";
		
	}
	
}
	
			

