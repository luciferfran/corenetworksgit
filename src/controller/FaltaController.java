package controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import entidades.Curso;
import entidades.Cursoalumno;
import entidades.Falta;
import entidades.FaltaRellenar;
import modelo.negocio.GestionFalta;
import modelo.negocio.GestionCursoalumno;

@Controller
@SessionAttributes(names={"profesor"})
public class FaltaController {
	@Autowired
	GestionFalta gestionF;
	@Autowired
	GestionCursoalumno gestionCa;
	@RequestMapping("/indexProfesorFaltas.htm")
	public String rellenarFormUsuario(@RequestParam("idCursoAlumno") int idCursoAlumno,Model modelo) {
		
		Falta falta = new Falta();
		Cursoalumno ca = new Cursoalumno();
		List<Falta> faltas = new ArrayList<Falta>();
		modelo.addAttribute("faltarellenar",new FaltaRellenar());
		modelo.addAttribute("falta",falta);
	
		if (idCursoAlumno==0) {
			modelo.addAttribute("Cursoalumno",null);
			modelo.addAttribute("Faltas",null);
		}
		else {
			ca=gestionCa.recuperarCursoalumno(idCursoAlumno);
			faltas= gestionF.recuperarFaltas(ca);
			modelo.addAttribute("Cursoalumno",ca);
			modelo.addAttribute("Faltas",faltas);		
		}
		return "indexProfesorFaltas";
	}
	@RequestMapping("/indexProfesorFaltasVer.htm")
	public String rellenarFormUsuarioFalta(@RequestParam("idCursoAlumno") int idCursoAlumno,@RequestParam("idFaltas") int idFaltas,Model modelo) {
		
		Falta falta = new Falta();
		Cursoalumno ca = new Cursoalumno();
		List<Falta> faltas = new ArrayList<Falta>();
		modelo.addAttribute("faltarellenar",new FaltaRellenar());
		modelo.addAttribute("falta",falta);
		
		if (idFaltas==0)
		{
			falta=null;
			modelo.addAttribute("Cursoalumno",null);
			modelo.addAttribute("Faltas",null);
		}
		else
		{
			falta=gestionF.recuperarFalta(idFaltas);
		}
		
		if (idCursoAlumno==0) {
			modelo.addAttribute("Cursoalumno",null);
			modelo.addAttribute("Faltas",null);
		}
		else {
			ca=gestionCa.recuperarCursoalumno(idCursoAlumno);
			faltas= gestionF.recuperarFaltas(ca);
			modelo.addAttribute("Cursoalumno",ca);
			modelo.addAttribute("Faltas",faltas);
		}
		
		return "indexProfesorFaltas";
	}
	
	@PostMapping("/indexProfesorFaltasVer.htm")
	public String registrarFalta(HttpServletRequest request, @ModelAttribute("faltarellenar")FaltaRellenar faltarellenar) {
		Cursoalumno ca = new Cursoalumno();
		ca = gestionCa.recuperarCursoalumno(faltarellenar.getIdCursoAlumno());
		
		Falta f= new Falta();
		f.setFecha(faltarellenar.getFecha());
		f.setHoras(faltarellenar.getHoras());
		f.setJustificado(faltarellenar.getJustificado());
		f.setMotivo(faltarellenar.getMotivo());
		f.setCursoalumno(ca);
		
		gestionF.registrarFalta(f);
		
		return "indexProfesorFaltas";
	}
}
