package entidades;

public class CursoAlumnoRellenar extends Cursoalumno{
	
	private int idAlumno;
	private int idCurso;
	public int getIdAlumno() {
		return idAlumno;
	}
	public void setIdAlumno(int idAlumno) {
		this.idAlumno = idAlumno;
	}
	public int getIdCurso() {
		return idCurso;
	}
	public void setIdCurso(int idCurso) {
		this.idCurso = idCurso;
	}

}
