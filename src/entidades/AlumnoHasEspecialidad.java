package entidades;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the alumno_has_especialidad database table.
 * 
 */
@Entity
@Table(name="alumno_has_especialidad")
@NamedQuery(name="AlumnoHasEspecialidad.findAll", query="SELECT a FROM AlumnoHasEspecialidad a")
public class AlumnoHasEspecialidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAlumnoEspecialidad;

	//bi-directional many-to-one association to Alumno
	@ManyToOne
	private Alumno alumno;

	//bi-directional many-to-one association to Especialidad
	@ManyToOne
	private Especialidad especialidad;

	public AlumnoHasEspecialidad() {
	}

	public int getIdAlumnoEspecialidad() {
		return this.idAlumnoEspecialidad;
	}

	public void setIdAlumnoEspecialidad(int idAlumnoEspecialidad) {
		this.idAlumnoEspecialidad = idAlumnoEspecialidad;
	}

	public Alumno getAlumno() {
		return this.alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Especialidad getEspecialidad() {
		return this.especialidad;
	}

	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}

}