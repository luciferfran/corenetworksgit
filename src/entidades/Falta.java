package entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the faltas database table.
 * 
 */
@Entity
@Table(name="faltas")
@NamedQuery(name="Falta.findAll", query="SELECT f FROM Falta f")
public class Falta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idFaltas;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	private int horas;

	private byte justificado;

	private String motivo;

	//bi-directional many-to-one association to Cursoalumno
	@ManyToOne
	private Cursoalumno cursoalumno;

	public Falta() {
	}

	public int getIdFaltas() {
		return this.idFaltas;
	}

	public void setIdFaltas(int idFaltas) {
		this.idFaltas = idFaltas;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getHoras() {
		return this.horas;
	}

	public void setHoras(int horas) {
		this.horas = horas;
	}

	public byte getJustificado() {
		return this.justificado;
	}

	public void setJustificado(byte justificado) {
		this.justificado = justificado;
	}

	public String getMotivo() {
		return this.motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Cursoalumno getCursoalumno() {
		return this.cursoalumno;
	}

	public void setCursoalumno(Cursoalumno cursoalumno) {
		this.cursoalumno = cursoalumno;
	}

}