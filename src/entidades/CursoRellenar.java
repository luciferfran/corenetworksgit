package entidades;

import org.springframework.web.multipart.MultipartFile;

public class CursoRellenar extends Curso {
	private static final long serialVersionUID = 1L;
	
	private int idEspecialidad;
	private int idProfesor;
	private MultipartFile subida;

	public int getIdEspecialidad() {
		return idEspecialidad;
	}

	public void setIdEspecialidad(int idEspecialidad) {
		this.idEspecialidad = idEspecialidad;
	}

	public int getIdProfesor() {
		return idProfesor;
	}

	public void setIdProfesor(int idProfesor) {
		this.idProfesor = idProfesor;
	}

	public MultipartFile getSubida() {
		return subida;
	}

	public void setSubida(MultipartFile subida) {
		this.subida = subida;
	}
}
