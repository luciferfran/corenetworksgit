package entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the alumno database table.
 * 
 */
@Entity
@NamedQuery(name="Alumno.findAll", query="SELECT a FROM Alumno a")
public class Alumno implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idalumno;

	private String apellidos;

	private String dni;

	private String email;

	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;

	private String nombre;

	private String pass;

	private String tlf;

	//bi-directional many-to-one association to AlumnoHasEspecialidad
	@OneToMany(mappedBy="alumno",fetch = FetchType.EAGER)
	private List<AlumnoHasEspecialidad> alumnoHasEspecialidads;

	//bi-directional many-to-one association to Cursoalumno
	@OneToMany(mappedBy="alumno")
	private List<Cursoalumno> cursoalumnos;

	public Alumno() {
	}

	public int getIdalumno() {
		return this.idalumno;
	}

	public void setIdalumno(int idalumno) {
		this.idalumno = idalumno;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return this.dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getTlf() {
		return this.tlf;
	}

	public void setTlf(String tlf) {
		this.tlf = tlf;
	}

	public List<AlumnoHasEspecialidad> getAlumnoHasEspecialidads() {
		return this.alumnoHasEspecialidads;
	}

	public void setAlumnoHasEspecialidads(List<AlumnoHasEspecialidad> alumnoHasEspecialidads) {
		this.alumnoHasEspecialidads = alumnoHasEspecialidads;
	}

	public AlumnoHasEspecialidad addAlumnoHasEspecialidad(AlumnoHasEspecialidad alumnoHasEspecialidad) {
		getAlumnoHasEspecialidads().add(alumnoHasEspecialidad);
		alumnoHasEspecialidad.setAlumno(this);

		return alumnoHasEspecialidad;
	}

	public AlumnoHasEspecialidad removeAlumnoHasEspecialidad(AlumnoHasEspecialidad alumnoHasEspecialidad) {
		getAlumnoHasEspecialidads().remove(alumnoHasEspecialidad);
		alumnoHasEspecialidad.setAlumno(null);

		return alumnoHasEspecialidad;
	}

	public List<Cursoalumno> getCursoalumnos() {
		return this.cursoalumnos;
	}

	public void setCursoalumnos(List<Cursoalumno> cursoalumnos) {
		this.cursoalumnos = cursoalumnos;
	}

	public Cursoalumno addCursoalumno(Cursoalumno cursoalumno) {
		getCursoalumnos().add(cursoalumno);
		cursoalumno.setAlumno(this);

		return cursoalumno;
	}

	public Cursoalumno removeCursoalumno(Cursoalumno cursoalumno) {
		getCursoalumnos().remove(cursoalumno);
		cursoalumno.setAlumno(null);

		return cursoalumno;
	}

}