package entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the especialidad database table.
 * 
 */
@Entity
@NamedQuery(name="Especialidad.findAll", query="SELECT e FROM Especialidad e")
public class Especialidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idEspecialidad;

	private String nombre;

	//bi-directional many-to-one association to AlumnoHasEspecialidad
	@OneToMany(mappedBy="especialidad")
	private List<AlumnoHasEspecialidad> alumnoHasEspecialidads;

	//bi-directional many-to-one association to Curso
	@OneToMany(mappedBy="especialidad")
	private List<Curso> cursos;

	//bi-directional many-to-many association to Profesor
	@ManyToMany(mappedBy="especialidads")
	private List<Profesor> profesors;

	public Especialidad() {
	}

	public int getIdEspecialidad() {
		return this.idEspecialidad;
	}

	public void setIdEspecialidad(int idEspecialidad) {
		this.idEspecialidad = idEspecialidad;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<AlumnoHasEspecialidad> getAlumnoHasEspecialidads() {
		return this.alumnoHasEspecialidads;
	}

	public void setAlumnoHasEspecialidads(List<AlumnoHasEspecialidad> alumnoHasEspecialidads) {
		this.alumnoHasEspecialidads = alumnoHasEspecialidads;
	}

	public AlumnoHasEspecialidad addAlumnoHasEspecialidad(AlumnoHasEspecialidad alumnoHasEspecialidad) {
		getAlumnoHasEspecialidads().add(alumnoHasEspecialidad);
		alumnoHasEspecialidad.setEspecialidad(this);

		return alumnoHasEspecialidad;
	}

	public AlumnoHasEspecialidad removeAlumnoHasEspecialidad(AlumnoHasEspecialidad alumnoHasEspecialidad) {
		getAlumnoHasEspecialidads().remove(alumnoHasEspecialidad);
		alumnoHasEspecialidad.setEspecialidad(null);

		return alumnoHasEspecialidad;
	}

	public List<Curso> getCursos() {
		return this.cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	public Curso addCurso(Curso curso) {
		getCursos().add(curso);
		curso.setEspecialidad(this);

		return curso;
	}

	public Curso removeCurso(Curso curso) {
		getCursos().remove(curso);
		curso.setEspecialidad(null);

		return curso;
	}

	public List<Profesor> getProfesors() {
		return this.profesors;
	}

	public void setProfesors(List<Profesor> profesors) {
		this.profesors = profesors;
	}

}