package entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the profesor database table.
 * 
 */
@Entity
@NamedQuery(name="Profesor.findAll", query="SELECT p FROM Profesor p")
public class Profesor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idProfesor;

	private String apellidos;

	@Lob
	private String cv;

	private String email;

	private String nombre;

	private String pass;

	private String tlf;

	//bi-directional many-to-one association to Curso
	@OneToMany(mappedBy="profesor")
	private List<Curso> cursos;

	//bi-directional many-to-many association to Especialidad
	@ManyToMany
	@JoinTable(
		name="profesor_has_especialidad"
		, joinColumns={
			@JoinColumn(name="profesor_idProfesor")
			}
		, inverseJoinColumns={
			@JoinColumn(name="especialidad_idEspecialidad")
			}
		)
	private List<Especialidad> especialidads;

	public Profesor() {
	}

	public int getIdProfesor() {
		return this.idProfesor;
	}

	public void setIdProfesor(int idProfesor) {
		this.idProfesor = idProfesor;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCv() {
		return this.cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getTlf() {
		return this.tlf;
	}

	public void setTlf(String tlf) {
		this.tlf = tlf;
	}

	public List<Curso> getCursos() {
		return this.cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	public Curso addCurso(Curso curso) {
		getCursos().add(curso);
		curso.setProfesor(this);

		return curso;
	}

	public Curso removeCurso(Curso curso) {
		getCursos().remove(curso);
		curso.setProfesor(null);

		return curso;
	}

	public List<Especialidad> getEspecialidads() {
		return this.especialidads;
	}

	public void setEspecialidads(List<Especialidad> especialidads) {
		this.especialidads = especialidads;
	}

}