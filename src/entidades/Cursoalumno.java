package entidades;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.IndexColumn;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;


/**
 * The persistent class for the cursoalumno database table.
 * 
 */
@Entity
@NamedQuery(name="Cursoalumno.findAll", query="SELECT c FROM Cursoalumno c")
public class Cursoalumno implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idCursoAlumno;

	private byte baja;

	private Timestamp fechaRegistro;

	private byte matriculado;

	private int nota;

	//bi-directional many-to-one association to Alumno
	@ManyToOne
	@JoinColumn(name="idAlumno")
	private Alumno alumno;

	//bi-directional many-to-one association to Curso
	@ManyToOne
	@JoinColumn(name="idCurso")
	private Curso curso;

	@OneToMany(mappedBy="cursoalumno",cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@IndexColumn(name="idFaltas")
	List<Falta> faltas = new LinkedList<Falta>();  
	//bi-directional many-to-one association to Falta
//	@OneToMany(mappedBy="cursoalumno",fetch = FetchType.EAGER)
//	private List<Falta> faltas;

	public Cursoalumno() {
	}

	public int getIdCursoAlumno() {
		return this.idCursoAlumno;
	}

	public void setIdCursoAlumno(int idCursoAlumno) {
		this.idCursoAlumno = idCursoAlumno;
	}

	public byte getBaja() {
		return this.baja;
	}

	public void setBaja(byte baja) {
		this.baja = baja;
	}

	public Timestamp getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public byte getMatriculado() {
		return this.matriculado;
	}

	public void setMatriculado(byte matriculado) {
		this.matriculado = matriculado;
	}

	public int getNota() {
		return this.nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	public Alumno getAlumno() {
		return this.alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Curso getCurso() {
		return this.curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<Falta> getFaltas() {
		return this.faltas;
	}

	public void setFaltas(List<Falta> faltas) {
		this.faltas = faltas;
	}

	public Falta addFalta(Falta falta) {
		getFaltas().add(falta);
		falta.setCursoalumno(this);

		return falta;
	}

	public Falta removeFalta(Falta falta) {
		getFaltas().remove(falta);
		falta.setCursoalumno(null);

		return falta;
	}

}