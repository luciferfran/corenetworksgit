package interceptores;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class InterceptorAutenticacion implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handle) throws Exception {
		// Comprueblo si existe atributo tipo de sesion
		if ((request.getSession().getAttribute("tipo") == null)) {
			// Si no hay exite el atributo pero va a esas 2 p�ginas le dejo trabajar
			if (request.getRequestURI().indexOf("login.htm") != -1
					|| request.getRequestURI().indexOf("registro.htm") != -1
					|| request.getRequestURI().indexOf("index.htm") != -1
					|| request.getRequestURI().indexOf("cerrarSesion.htm") != -1
					|| request.getRequestURI().indexOf("cargaInicial.htm") != -1) {
				return true;
			} else {
				response.sendRedirect("login.htm");
				return false;
			}
			// Aqu� si exite el atributo
		} else {
			// Como tiene sesion si va a cerrarla o al index le dejamos trabajar
			if (!(request.getRequestURI().indexOf("index.htm") == -1)
					|| !(request.getRequestURI().indexOf("cerrarSesion.htm") == -1))
				return true;
			else {
				if (request.getSession().getAttribute("tipo").equals("administrador")) {
					if (request.getRequestURI().indexOf("indexAdmin") != -1)
						return true;
				}
				if (request.getSession().getAttribute("tipo").equals("alumno")) {
					if (request.getRequestURI().indexOf("indexAlumno") != -1)
						return true;
				}
				if (request.getSession().getAttribute("tipo").equals("profesor")) {
					if (request.getRequestURI().indexOf("indexProfesor") != -1)
						return true;
				}
				response.sendRedirect("cerrarSesion.htm");
				return false;
			}
		}
	}
}