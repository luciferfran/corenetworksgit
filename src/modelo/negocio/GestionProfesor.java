package modelo.negocio;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entidades.Alumno;
import entidades.Profesor;
import modelo.dao.ProfesorRepo;

@Service
public class GestionProfesor {
	@Autowired
	ProfesorRepo repoP;

	@Transactional
	public boolean registrarProfesor(Profesor p) {
//		if (repoP.findByEmail(p.getEmail()) == null) {
			repoP.save(p);
			return true;
//		} else

	}

	public Profesor logarProfesor(Alumno user) {
		Profesor profesorRecogido = repoP.findByEmail(user.getEmail());
		if (profesorRecogido != null && user.getPass().equals(profesorRecogido.getPass()))
			return profesorRecogido;
		else
			return null;
	}


	public Profesor recuperarProfesor(int idProfesor) {
		return repoP.findOne(idProfesor);
	}
	
	public Profesor recuperarProfesor(String email) {
		return repoP.findByEmail(email);
	}
	
	public List<Profesor> recuperarProfesores() {
		return (List<Profesor>) repoP.findAll();
	}

	@Transactional
	public boolean modificarProfesor(String email) {
		Profesor modificar=repoP.findByEmail(email);
		repoP.save(modificar);
		return true;
	}
	
	@Transactional
	public boolean modificarProfesor(Profesor p) {
		repoP.save(p);
		return true;
	}	
	



}
