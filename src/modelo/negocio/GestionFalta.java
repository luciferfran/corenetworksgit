package modelo.negocio;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entidades.Cursoalumno;
import entidades.Falta;
import modelo.dao.FaltaRepo;
@Service
public class GestionFalta {
	@Autowired
	FaltaRepo repoF;
	@Transactional
	public boolean registrarFalta(Falta f) {
		repoF.save(f);
		return true;
	}
	
	public Falta recuperarFalta(int idFaltas) {
		return repoF.findOne(idFaltas);
	}
	
	public List<Falta>  recuperarFaltas(Cursoalumno cursoalumno) {
		return (List<Falta>) repoF.findByCursoalumno(cursoalumno);
	}
}

