package modelo.negocio;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entidades.Alumno;
import entidades.Curso;
import entidades.Cursoalumno;

import modelo.dao.CursoalumnoRepo;

@Service
public class GestionCursoalumno {
	@Autowired
	CursoalumnoRepo repoCa;
	@Autowired
	GestionCurso gc;
	@Autowired
	GestionFalta gf;
	
	@Transactional
	public Cursoalumno registrarCursoalumno(Cursoalumno matricula) {
		return repoCa.save(matricula);
	} 
	
	public Cursoalumno recuperarCursoalumno(int idCursoAlumno) {
		return repoCa.findOne(idCursoAlumno); 
	}
	
	public int recuperarNumFaltas(Cursoalumno cursoalumno) {
		return gf.recuperarFaltas(cursoalumno).size();
	}
	
	

//	public List<Curso> numeroMatriculasTotales(int idCurso) {
//		// TODO Auto-generated method stub
//		return repoCa.findAllByIdCurso(idCurso);
//	}
}
