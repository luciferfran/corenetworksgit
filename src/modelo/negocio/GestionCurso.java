package modelo.negocio;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entidades.Alumno;
import entidades.Curso;
import entidades.Cursoalumno;
import entidades.Profesor;
import modelo.dao.CursoRepo;

@Service
public class GestionCurso {

	@Autowired
	CursoRepo repoC;
	@Autowired
	GestionCursoalumno gCa;
	
	public List<Curso> recuperarCursos() {
		return (List<Curso>) repoC.findAll();
	}
	public List<Curso> recuperarCursosProfesor(Profesor profe) {
		return (List<Curso>) repoC.findByProfesor(profe);
	}
	@Transactional
	public void registrarCurso(Curso curso) {
		 repoC.save(curso);
	}
	
	public Curso recuperarCurso(int id) {
		return repoC.findOne(id);
	}

	public Curso recuperarCurso(String nombre) {
		return repoC.findByNombre(nombre);
	}
	
		

}
