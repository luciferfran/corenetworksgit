package modelo.negocio;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entidades.Alumno;
import entidades.AlumnoHasEspecialidad;
import modelo.dao.AlumnoHasEspecialidadRepo;


@Service
public class GestionAlumnoHasEspecialidad {
	@Autowired
	AlumnoHasEspecialidadRepo repoA;
	

	@Transactional
	public boolean registrarAlumnoHasEspecialidad(AlumnoHasEspecialidad a) {
		repoA.save(a);
		return true;
	}
	
	@Transactional
	public boolean deleteByAlumno(Alumno alumno ) {
		
		repoA.deleteByAlumno(alumno);
		return true;
	}


}
