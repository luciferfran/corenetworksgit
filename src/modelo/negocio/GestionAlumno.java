package modelo.negocio;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import entidades.Alumno;
import entidades.Cursoalumno;

import modelo.dao.AlumnoRepo;
import modelo.dao.CursoalumnoRepo;


@Service
public class GestionAlumno {

	@Autowired
	AlumnoRepo repoA;
	

	@Transactional
	public boolean registrarAlumno(Alumno a) {
		if (repoA.findByEmail(a.getEmail()) == null) {
			repoA.save(a);
			return true;
		} else
			return false;
	}
	
	@Transactional
	public boolean modificarAlumno(Alumno a) {
		
			repoA.save(a);
			return true;
		
	}

	public Alumno logarAlumno(Alumno user) {
		Alumno alumnoRecogido = repoA.findByEmail(user.getEmail());
		if (alumnoRecogido != null && user.getPass().equals(alumnoRecogido.getPass()))
			return alumnoRecogido;
		else
			return null;
	}

	public Alumno recuperarAlumno(int idAlumno) {
		return repoA.findOne(idAlumno); 
	}
	public Alumno recuperarAlumno(String email) {
		return repoA.findByEmail(email);
	}

	public AlumnoRepo getRepoA() {
		return repoA;
	}

	public void setRepoA(AlumnoRepo repoA) {
		this.repoA = repoA;
	}

}
