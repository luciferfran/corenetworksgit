package modelo.negocio;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import entidades.Especialidad;

import modelo.dao.EspecialidadRepo;

@Service
public class GestionEspecialidad {

	@Autowired
	EspecialidadRepo repoE;
	
	@Transactional
	public boolean registrarEspecialidad(Especialidad e) {
		if (e.getNombre() != null) {
			repoE.save(e);
			return true;
		} else
			return false;
	}
	
	public List<Especialidad> recuperarEspecialidades() {
		return (List<Especialidad>) repoE.findAll();
	}

	public Especialidad recuperarEspecialidad(String nombre) {
		return repoE.findByNombre(nombre);
	}
	
	public Especialidad recuperarEspecialidad(int id) {
		return repoE.findOne(id);
	}
}
