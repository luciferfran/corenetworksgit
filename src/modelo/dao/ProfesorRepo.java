package modelo.dao;

import org.springframework.data.repository.CrudRepository;

import entidades.Profesor;



public interface ProfesorRepo extends CrudRepository<Profesor, Integer> {

	Profesor findByEmail(String email);


	

}
