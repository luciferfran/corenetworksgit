package modelo.dao;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import entidades.Cursoalumno;
import entidades.Falta;


public interface FaltaRepo extends CrudRepository<Falta, Integer> {

	List<Falta> findByCursoalumno(Cursoalumno cursoalumno);

	
}

 

 