package modelo.dao;

import org.springframework.data.repository.CrudRepository;

import entidades.Alumno;
import entidades.Cursoalumno;

public interface AlumnoRepo extends CrudRepository<Alumno, Integer> {

	Alumno findByEmail(String email);

	

}
