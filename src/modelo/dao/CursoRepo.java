package modelo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import entidades.Curso;
import entidades.Profesor;

public interface CursoRepo extends CrudRepository<Curso, Integer>{

	Curso findByNombre(String nombre);

	List<Curso> findByProfesor(Profesor profe);

	
}
