package modelo.dao;

import org.springframework.data.repository.CrudRepository;

import entidades.Alumno;
import entidades.AlumnoHasEspecialidad;

public interface AlumnoHasEspecialidadRepo extends CrudRepository<AlumnoHasEspecialidad, Integer> {

	void deleteByAlumno(Alumno alumno);


}
