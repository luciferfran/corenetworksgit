package modelo.dao;

import org.springframework.data.repository.CrudRepository;

import entidades.Especialidad;
import entidades.Profesor;

public interface EspecialidadRepo extends CrudRepository<Especialidad, Integer>{

	Especialidad findByNombre(String nombre);

}

