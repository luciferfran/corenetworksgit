-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`alumno`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`alumno` ;

CREATE TABLE IF NOT EXISTS `mydb`.`alumno` (
  `idalumno` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellidos` VARCHAR(45) NOT NULL,
  `fechaNacimiento` DATE NULL,
  `tlf` VARCHAR(9) NULL,
  `dni` VARCHAR(9) NULL,
  `email` VARCHAR(45) NOT NULL,
  `pass` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idalumno`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`profesor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`profesor` ;

CREATE TABLE IF NOT EXISTS `mydb`.`profesor` (
  `idProfesor` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellidos` VARCHAR(45) NOT NULL,
  `cv` TEXT(2048) NOT NULL,
  `tlf` VARCHAR(9) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `pass` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idProfesor`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`especialidad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`especialidad` ;

CREATE TABLE IF NOT EXISTS `mydb`.`especialidad` (
  `idEspecialidad` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`idEspecialidad`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`curso`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`curso` ;

CREATE TABLE IF NOT EXISTS `mydb`.`curso` (
  `idCurso` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `duracion` VARCHAR(45) NOT NULL,
  `contenido` TEXT(1024) NOT NULL,
  `idEspecialidad` INT NOT NULL,
  `lugar` VARCHAR(45) NOT NULL,
  `fechaInicio` DATETIME NOT NULL,
  `fechaFin` DATETIME NOT NULL,
  `profesor_idProfesor` INT NOT NULL,
  `imagen` VARCHAR(256) NULL,
  PRIMARY KEY (`idCurso`),
  INDEX `idEspecialidad_idx` (`idEspecialidad` ASC),
  INDEX `fk_curso_profesor1_idx` (`profesor_idProfesor` ASC),
  CONSTRAINT `idEspecialidad`
    FOREIGN KEY (`idEspecialidad`)
    REFERENCES `mydb`.`especialidad` (`idEspecialidad`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_curso_profesor1`
    FOREIGN KEY (`profesor_idProfesor`)
    REFERENCES `mydb`.`profesor` (`idProfesor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`cursoAlumno`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`cursoAlumno` ;

CREATE TABLE IF NOT EXISTS `mydb`.`cursoAlumno` (
  `idCursoAlumno` INT NOT NULL AUTO_INCREMENT,
  `idCurso` INT NOT NULL,
  `idAlumno` INT NOT NULL,
  `nota` INT NOT NULL,
  `baja` TINYINT NOT NULL DEFAULT 0,
  `matriculado` TINYINT NULL DEFAULT 1,
  `fechaRegistro` TIMESTAMP(3) NULL,
  PRIMARY KEY (`idCursoAlumno`),
  INDEX `idCurso_idx` (`idCurso` ASC),
  INDEX `idAlumno_idx` (`idAlumno` ASC),
  CONSTRAINT `idCurso`
    FOREIGN KEY (`idCurso`)
    REFERENCES `mydb`.`curso` (`idCurso`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `idAlumno`
    FOREIGN KEY (`idAlumno`)
    REFERENCES `mydb`.`alumno` (`idalumno`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`profesor_has_especialidad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`profesor_has_especialidad` ;

CREATE TABLE IF NOT EXISTS `mydb`.`profesor_has_especialidad` (
  `profesor_idProfesor` INT NOT NULL,
  `especialidad_idEspecialidad` INT NOT NULL,
  PRIMARY KEY (`profesor_idProfesor`, `especialidad_idEspecialidad`),
  INDEX `fk_profesor_has_especialidad_especialidad1_idx` (`especialidad_idEspecialidad` ASC),
  INDEX `fk_profesor_has_especialidad_profesor1_idx` (`profesor_idProfesor` ASC),
  CONSTRAINT `fk_profesor_has_especialidad_profesor1`
    FOREIGN KEY (`profesor_idProfesor`)
    REFERENCES `mydb`.`profesor` (`idProfesor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_profesor_has_especialidad_especialidad1`
    FOREIGN KEY (`especialidad_idEspecialidad`)
    REFERENCES `mydb`.`especialidad` (`idEspecialidad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`alumno_has_especialidad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`alumno_has_especialidad` ;

CREATE TABLE IF NOT EXISTS `mydb`.`alumno_has_especialidad` (
  `alumno_idalumno` INT NOT NULL,
  `especialidad_idEspecialidad` INT NOT NULL,
  PRIMARY KEY (`alumno_idalumno`, `especialidad_idEspecialidad`),
  INDEX `fk_alumno_has_especialidad_especialidad1_idx` (`especialidad_idEspecialidad` ASC),
  INDEX `fk_alumno_has_especialidad_alumno1_idx` (`alumno_idalumno` ASC),
  CONSTRAINT `fk_alumno_has_especialidad_alumno1`
    FOREIGN KEY (`alumno_idalumno`)
    REFERENCES `mydb`.`alumno` (`idalumno`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alumno_has_especialidad_especialidad1`
    FOREIGN KEY (`especialidad_idEspecialidad`)
    REFERENCES `mydb`.`especialidad` (`idEspecialidad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`faltas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`faltas` ;

CREATE TABLE IF NOT EXISTS `mydb`.`faltas` (
  `idFaltas` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATE NOT NULL,
  `horas` INT NOT NULL,
  `cursoAlumno_idCursoAlumno` INT NOT NULL,
  `motivo` VARCHAR(45) NULL,
  `justificado` TINYINT NULL,
  PRIMARY KEY (`idFaltas`),
  INDEX `fk_faltas_cursoAlumno1_idx` (`cursoAlumno_idCursoAlumno` ASC),
  CONSTRAINT `fk_faltas_cursoAlumno1`
    FOREIGN KEY (`cursoAlumno_idCursoAlumno`)
    REFERENCES `mydb`.`cursoAlumno` (`idCursoAlumno`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
